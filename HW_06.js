/**
 * Created by Алина on 15.01.2022.
 */

// Теоретический вопрос
    // 1. Опишите своими словами как работает цикл forEach.
    // Цикл forEach перебирает последовательно каждый элемент массива и выполняет указанную функцию callback
    // для каждого из этих элементов. Этот метод не создает новый массив.

const allTypes = ["string", "number", "boolean", "null", "undefined", "object"]
let customArr = ['hello', 'world', 23, '23', null, undefined, {}, false, true];
function filterBy(array, type) {
    let newArr = array.filter(function (item) {
        if (item === null && type === "null") {
            return false
        } else if (typeof item !== type) {
            return true
        }
        return false
    })
    return newArr
}

allTypes.forEach(type => console.log(filterBy(customArr, type)))

// console.log(filterBy(customArr, "string"));
// console.log(filterBy(customArr, "number"));
// console.log(filterBy(customArr, "boolean"));
// console.log(filterBy(customArr, "null"));
// console.log(filterBy(customArr, "object"));
// console.log(filterBy(customArr, "undefined"));

